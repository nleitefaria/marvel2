# Marvel RESTful adapter

#### This project is a POC for Growin. It's a Spring Boot project.
#### 
#### It consumes the Marvel RESTful API, performs some transformations and exposes two end points:
##### Endpoint 1 : (GET) : http://localhost:8080/characters
##### Endpoint 2 : (GET) : http://localhost:8080/characters/characterId?language=languageCode
#### 
# How to run the project
##### 1. mvn clean install
##### 2. mvn spring-boot:run
##### 3. The API documentation (SWAGGER UI) can be found in http://localhost:8080