package com.nleitefaria.marvel.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CharacterMarvelResourceTest {
	
	@Autowired
	private CharacterMarvelResource characterMarvelResource;

	@Test
	public void contextLoads() throws Exception {
		assertThat(characterMarvelResource).isNotNull();
	}

}
