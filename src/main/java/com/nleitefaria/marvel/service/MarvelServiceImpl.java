package com.nleitefaria.marvel.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nleitefaria.marvel.dto.CharacterDTO;
import com.nleitefaria.marvel.model.CharacterDataWrapper;
import com.nleitefaria.marvel.util.TokenUtil;
import com.nleitefaria.marvel.util.TranslationUtil;

@Service
public class MarvelServiceImpl implements MarvelService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	TokenUtil tokenUtil;

	@Autowired
	TranslationUtil translationUtil;

	@Autowired
	private Environment env;

	@Autowired
	private AESDecryptionService decryptionService;

	public List<Integer> getAllCharactersIds() {
		logger.info("Getting all CharactersIds");
		List<Integer> allIdsdList = new ArrayList<>();
		File f = new File("iddata.ser");
		// If the cache file already exists.
		if (f.exists() && !f.isDirectory()) {
			logger.info("File already exists. Getting data from the file");
			allIdsdList = readFromFile();
		} else {
			// If the cache file does not exists yet.
			logger.info("Cache file does not exists yet. Calling the Marvel API");
			try (CloseableHttpClient client = HttpClients.createDefault()) {
				float offset = 0;
				float total = 1;
				while (offset < total) {
					String ts = Long.toString(System.currentTimeMillis());
					URI uri = new URIBuilder(env.getProperty("marvel.api.url")).addParameter("limit", "100")
							.addParameter("offset", Float.toString(offset))
							.addParameter("apikey", decryptionService.decrypt(env.getProperty("pub.key.enc"), env.getProperty("sec.sec")))
							.addParameter("ts", ts)
							.addParameter("hash", DigestUtils.md5Hex(ts + decryptionService.decrypt(env.getProperty("pri.key.enc"), env.getProperty("sec.sec")) + decryptionService.decrypt(env.getProperty("pub.key.enc"),env.getProperty("sec.sec"))))
							.build();
					HttpGet request = new HttpGet(uri);

					try (CloseableHttpResponse response = client.execute(request)) {
						if (response.getStatusLine().getStatusCode() != 200) {
							throw new RuntimeException(
									"Connection error; status code [" + response.getStatusLine().getStatusCode() + "]");
						}
						HttpEntity entity = response.getEntity();
						if (entity == null) {
							throw new RuntimeException(
									"Response entity is null. Response: [" + response.toString() + "].");
						}
						String result = EntityUtils.toString(entity);
						ObjectMapper mapper = new ObjectMapper();
						CharacterDataWrapper characterDataWrapper = mapper.readValue(result,
								CharacterDataWrapper.class);
						total = characterDataWrapper.getData().getTotal();
						offset += characterDataWrapper.getData().getCount();

						int characterId = 0;
						for (com.nleitefaria.marvel.model.Character character : characterDataWrapper.getData().getResults()) {
							characterId = (int) character.getId();
							allIdsdList.add((int) character.getId());
							logger.info("Adding character with id: " + characterId);
						}
					} catch (ClientProtocolException e) {
						logger.info("An exception ocurred");
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				logger.info("An exception ocurred");
				e.printStackTrace();
			}
			Collections.sort(allIdsdList);
			writeToFile(allIdsdList);
		}
		logger.info("Done getting all CharactersIds");

		return allIdsdList;
	}

	public CharacterDTO findById(Integer id, String language) {
		CharacterDTO characterDTO = null;
		try (CloseableHttpClient client = HttpClients.createDefault()) {
			String ts = Long.toString(System.currentTimeMillis());
			URI uri = new URIBuilder(env.getProperty("marvel.api.url") + "/" + Integer.toString(id))
					.addParameter("apikey",decryptionService.decrypt(env.getProperty("pub.key.enc"), env.getProperty("sec.sec")))
					.addParameter("ts", ts)
					.addParameter("hash", DigestUtils.md5Hex(ts + decryptionService.decrypt(env.getProperty("pri.key.enc"), env.getProperty("sec.sec")) + decryptionService.decrypt(env.getProperty("pub.key.enc"), env.getProperty("sec.sec"))))
					.build();
			HttpGet request = new HttpGet(uri);

			try (CloseableHttpResponse response = client.execute(request)) {
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException(
							"Connection error; status code [" + response.getStatusLine().getStatusCode() + "]");
				}
				HttpEntity entity = response.getEntity();
				if (entity == null) {
					throw new RuntimeException("Response entity is null. Response: [" + response.toString() + "].");
				}
				String result = EntityUtils.toString(entity);
				ObjectMapper mapper = new ObjectMapper();
				CharacterDataWrapper characterDataWrapper = mapper.readValue(result, CharacterDataWrapper.class);

				com.nleitefaria.marvel.model.Character character = characterDataWrapper.getData().getResults().get(0);

				if (language == null || language.isEmpty()) {
					characterDTO = new CharacterDTO((float) character.getId(), character.getName(),
							character.getDescription(), character.getThumbnail());
				} else {
					// TODO: When the translation service is finished uncomment the line bellow
					// characterDTO = new CharacterDTO(character.getId(), translationUtil.translate(character.getName(), language), translationUtil.translate(character.getDescription(), language), character.getThumbnail());
					characterDTO = new CharacterDTO(character.getId(), character.getName(), character.getDescription(), character.getThumbnail());
				}

			} catch (ClientProtocolException e) {
				logger.info("An exception ocurred");
				e.printStackTrace();
			}

		} catch (Exception e) {
			logger.info("An exception ocurred");
			e.printStackTrace();
		}
		return characterDTO;
	}

	private void writeToFile(List<Integer> idsList) {
		try {
			FileOutputStream writeData = new FileOutputStream("iddata.ser");
			ObjectOutputStream writeStream = new ObjectOutputStream(writeData);
			writeStream.writeObject(idsList);
			writeStream.flush();
			writeStream.close();
		} catch (IOException e) {
			logger.info("An exception ocurred");
			e.printStackTrace();
		}
	}

	private List<Integer> readFromFile() {
		List<Integer> idList = new ArrayList<Integer>();
		try {
			FileInputStream readData = new FileInputStream("iddata.ser");
			ObjectInputStream readStream = new ObjectInputStream(readData);
			idList = (ArrayList<Integer>) readStream.readObject();
			readStream.close();
			System.out.println(idList.toString());
		} catch (Exception e) {
			logger.info("An exception ocurred");
			e.printStackTrace();
		}
		return idList;
	}
}
