package com.nleitefaria.marvel.service;

public interface AESDecryptionService {
    
    String decrypt(String strToDecrypt, String secret);

}
