package com.nleitefaria.marvel.service;

import java.util.List;

import com.nleitefaria.marvel.dto.CharacterDTO;

public interface MarvelService {
	
	List<Integer> getAllCharactersIds();
    CharacterDTO findById(Integer id, String language);
    
}
