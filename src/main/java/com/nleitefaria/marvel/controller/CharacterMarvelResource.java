package com.nleitefaria.marvel.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nleitefaria.marvel.dto.CharacterDTO;
import com.nleitefaria.marvel.service.MarvelServiceImpl;

@RestController
public class CharacterMarvelResource {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    MarvelServiceImpl marvelService;
      
    @GetMapping("/characters")
    public ResponseEntity<?> findAll() {
    	try {
    		List<Integer> idsList = new ArrayList<>();
    		idsList = marvelService.getAllCharactersIds();
    		return ResponseEntity.ok(idsList);
    	}catch (Exception e) {
    		logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
		}
    }
   
    @GetMapping("characters/{characterId}")
    public ResponseEntity<?> findCharacterById(@PathVariable(value = "characterId") Integer characterId, @RequestParam(name = "language", required = false) String language) {
    	try {
    		CharacterDTO characterDTO = marvelService.findById(characterId, language);
    		return ResponseEntity.ok().body(characterDTO);
    	}catch (Exception e) {
    		logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
		}
    }  
}
