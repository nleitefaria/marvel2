package com.nleitefaria.marvel.controller;

import com.nleitefaria.marvel.util.POMReaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiInfoResource {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	POMReaderUtil pomReaderUtil;
	
	@GetMapping("/info")
    public ResponseEntity<?> getInfo() {
    	try {
    		List<?> infoList = pomReaderUtil.getAPIInfo();
    		return ResponseEntity.ok(infoList);
    	}catch (Exception e) {
    		logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
		}
    }

}
