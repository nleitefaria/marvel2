package com.nleitefaria.marvel.util;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class POMReaderUtil {

    public List<?> getAPIInfo() {
        List<String> infoList = new ArrayList<>();
        MavenXpp3Reader reader = new MavenXpp3Reader();
        try {
            Model model = reader.read(new FileReader("pom.xml"));
            infoList.add(model.getId());
            infoList.add(model.getGroupId());
            infoList.add(model.getArtifactId());
            infoList.add(model.getVersion());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return infoList;
    }

}
