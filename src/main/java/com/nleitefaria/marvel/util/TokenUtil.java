package com.nleitefaria.marvel.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.arnaudpiroelle.marvel.api.MarvelApi;
import com.nleitefaria.marvel.service.AESDecryptionService;

@Component
public class TokenUtil {
    
    @Autowired
    private Environment env;
    
    @Autowired
    private AESDecryptionService decryptionService;
  
    public void refreshToken() {   
        MarvelApi.configure().withApiKeys(decryptionService.decrypt(env.getProperty("pub.key.enc"), env.getProperty("sec.sec")), decryptionService.decrypt(env.getProperty("pri.key.enc"), env.getProperty("sec.sec"))).init();
    }
}
