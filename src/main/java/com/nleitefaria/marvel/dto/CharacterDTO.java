package com.nleitefaria.marvel.dto;

import com.nleitefaria.marvel.model.Thumbnail;

public class CharacterDTO {
    
    private float id; 
    private String name;
    private String description;
    private Thumbnail thumbnail;
    
    public CharacterDTO() {
        super();
    }

    public CharacterDTO(float id, String name, String description, Thumbnail thumbnail) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.thumbnail = thumbnail;
	}

    public float getId() {
		return id;
	}

	public void setId(float id) {
		this.id = id;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}  
}
